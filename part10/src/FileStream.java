import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileStream {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        String filename = "/home/anas/IdeaProjects/mooc/part10/src/presidents.txt";

        try {

            Files.lines(Paths.get(filename))

                    .map(s -> s.split(";"))

                    .filter(p -> p.length >= 2)

                    .map(p -> (new Person(p[0], Integer.parseInt(p[1].trim()))))

                    .forEach(list::add);

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }

        //System.out.println(list);

        for (Person p : list) {
            System.out.println(p.getName() + " " + p.getYear() );
        }
    }
}
