import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> inputs = new ArrayList<>();

// reading inputs
//        while (true) {
//            String row = scanner.nextLine();
//            if (row.equals("end")) {
//                break;
//            }
//
//            inputs.add(row);
//        }
//
//        long numbersDivisibleByThree = inputs.stream()
//                .mapToInt(s -> Integer.valueOf(s))
//                .filter(n -> n % 3 == 0)
//                .count();
//
//        double average;
////        System.out.println("Print the average of the negative numbers or the positive numbers? (n/p)");
//        String a = scanner.nextLine();
//        if(a.equals("n")){
//            average = inputs.stream()
//                    .mapToInt(s -> Integer.valueOf(s))
//                    .filter(n -> n < 0)
//                    .average()
//                    .getAsDouble();
//
//        }
//        else{
//            average = inputs.stream()
//                    .mapToInt(s -> Integer.valueOf(s))
//                    .filter(n -> n > 0)
//                    .average()
//                    .getAsDouble();
//
//        }
//
//        Double greaterThanFiveAverage = inputs.stream()
//                .mapToInt(s -> Integer.valueOf(s))
//                .filter(Screeners::greaterThanFive)
//                .average()
//                .getAsDouble();

        //System.out.println("Divisible by three " + numbersDivisibleByThree);
        //System.out.println("Average number: " + average);
        //System.out.println("Average greater than five " + greaterThanFiveAverage);

        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(7);
        list.add(4);
        list.add(2);
        list.add(6);

        ArrayList<Integer> i = list.stream()
                .filter(value -> value > 5)
                .map(value -> value * 2)
                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println(i);

        System.out.println("Vlues: " + list.stream().count());

        list.stream()
                .forEach(s -> System.out.println(s));
    }
}
