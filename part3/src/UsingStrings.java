import java.util.Scanner;

public class UsingStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //comparing strings
        {
            String s1 = "hello";
            String s2 = "hell";

            //compare strings
            if (s1.equals(s2))
                System.out.println("same");
            else
                System.out.println("not same");
        }

        {
            String s = "This is test string";
            String[] str = s.split(" ");

            for(String i: str) System.out.println(i);
        }
    }
}
