import java.util.ArrayList;
import java.util.Scanner;

public class UsingArrayList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<String> list = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        list.add("Google");
        list.add("Amazon");
        list.add("Facebook");
        list.add("LVMH");

        list2.add(1);
        list2.add(2);
        list2.add(4);
        list2.add(7);

        System.out.println("Size: " + list.size());
        System.out.println("Size: " + list2.size());
        //normal for
      //  for(int i = 0; i < list.size(); i++)
        //    System.out.println(list.get(i));

        //for-each
        for(String s: list)
            System.out.println(s);

        for(Integer i : list2) System.out.println(i);

        list2.remove(2);
        for(Integer i : list2) System.out.println(i);


        list2.remove(Integer.valueOf(7));
        for(Integer i : list2) System.out.println(i);

        list.remove("Google");
        for(String i : list) System.out.println(i);


        if(list.contains("LVMH"))
            System.out.println("List contains LV");

        if(list2.contains(7))
            System.out.println("List contains 7");

        System.out.println(list);
        System.out.println(list2);
    }
}
