import java.util.Scanner;

public class UsingArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arr = new int[3];
        String[] str = new String[4];

        arr[0] = 4;
        arr[1] = 5;
        arr[2] = 6;

        str[0] = "This";
        str[1] = "is";
        str[2] = "a";
        str[3] = "Test";

        //for(int i:arr) System.out.println(i);

        //Swap
//        {
//            System.out.println("enter[0-2]: ");
//            int i1 = Integer.parseInt(scanner.nextLine());
//            int i2 = Integer.parseInt(scanner.nextLine());
//            int temp;
//
//            temp = arr[i1];
//            arr[i1] = arr[i2];
//            arr[i2] = temp;
//            for (int i : arr) System.out.println(i);
//        }

       // for(String i:str) System.out.print(i + " ");
        displayString(str);
        displayInt(arr);

        System.out.println("Size of arr: " + arr.length);
        System.out.println("Size of str: " + str.length);
    }

    public static void displayString(String[] s){
        for (String i:
             s) {
            System.out.print(i + " ");
        }
    }
    public static void displayInt(int[] s){
        for (int i:
                s) {
            System.out.print(i);
        }
    }
}
