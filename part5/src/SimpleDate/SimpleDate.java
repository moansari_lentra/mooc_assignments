package SimpleDate;

public class SimpleDate {
    int day;
    int month;
    int year;

    public SimpleDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public boolean equals(Object obj) {
        //
        if(this == obj)
            return true;

        //
        if(!(obj instanceof SimpleDate))
            return false;

        SimpleDate other = (SimpleDate) obj;

        if(this.day == other.day &&
        this.month == other.month &&
        this.year == other.year){
            return true;
        }

        return false;
    }

    @Override
    public String toString(){
        return this.day + "." + this.month + "." + this.year;
    }
}
