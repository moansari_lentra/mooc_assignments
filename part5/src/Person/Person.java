package Person;

import SimpleDate.SimpleDate;

public class Person {
    String name;
    SimpleDate dateOfBirth;
    int height;
    int weight;

    public Person(String name, SimpleDate dateOfBirth, int height, int weight) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.height = height;
        this.weight = weight;
    }

    public boolean equals(Object obj) {
        if(this == obj)
            return true;

        if(!(obj instanceof Person))
            return false;

        Person other = (Person) obj;
        if(this.name.equals(other.name) &&
        this.dateOfBirth.equals(other.dateOfBirth) &&
        this.height == other.height &&
        this.weight == other.weight){
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.name + " born on " + this.dateOfBirth + " is " + this.height + " cms tall and weighs " + this.weight + " kgs ";
    }
}
