package DatingApp;

public class SimpleDate {
    int day;
    int month;
    int year;

    public SimpleDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public boolean equals(Object obj) {
        //
        if(this == obj)
            return true;

        //
        if(!(obj instanceof SimpleDate))
            return false;

        SimpleDate other = (SimpleDate) obj;

        if(this.day == other.day &&
                this.month == other.month &&
                this.year == other.year){
            return true;
        }

        return false;
    }

    public void advance(){
        this.advance(1);
    }
    public void advance(int howManyDays) {
        this.day += howManyDays;
        if(this.day > 30){
            this.month += (this.day / 30);
            this.day = this.day % 30;
            if(this.month > 12){
                this.year +=(this.month / 12);
                this.month = this.month % 12;
            }
        }
    }
    public SimpleDate afterNumberOfDays(int days)
    {
        SimpleDate newDate = new SimpleDate(this.day,this.month,this.year);
        newDate.advance(days);
        return newDate;
    }
    @Override
    public String toString(){
        return this.day + "." + this.month + "." + this.year;
    }
}
