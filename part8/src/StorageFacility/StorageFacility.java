package StorageFacility;

import java.util.ArrayList;
import java.util.HashMap;

public class StorageFacility {
    HashMap<String, ArrayList<String>> hash;

    public StorageFacility(){
        this.hash = new HashMap<>();
    }

    public void add(String unit,String item){
        this.hash.putIfAbsent(unit,new ArrayList<String>());

        this.hash.get(unit).add(item);
    }

    public ArrayList<String> contents(String storageUnit){
        ArrayList<String> strings;

        strings = this.hash.getOrDefault(storageUnit,null);
        return strings;
    }
}
