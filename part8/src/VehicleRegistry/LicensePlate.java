package VehicleRegistry;

public class LicensePlate {
    private final String liNumber;
    private final String country;

    public LicensePlate(String country, String liNumber) {
        this.liNumber = liNumber;
        this.country = country;
    }

    public boolean equals(Object obj) {
        if(this == obj)
            return true;

        if(!(obj instanceof LicensePlate))
            return false;

        LicensePlate other = (LicensePlate) obj;

        return this.liNumber.equals(other.liNumber) &&
                this.country.equals(other.country);
    }

    public int hashCode() {
        if(this.liNumber == null)
            return this.country.hashCode();

        return this.country.hashCode() + this.liNumber.hashCode();
    }

    @Override
    public String toString() {
        return country + " " + liNumber;
    }
}
