package IOU;

import java.util.HashMap;

public class IOU {
    HashMap<String,Double> hashMap;

    public IOU(){
        hashMap = new HashMap<>();
    }

    public void setSum(String toWhom,double amount){

        this.hashMap.put(toWhom, amount);

    }

    public double howMuchDoIOweTo(String toWhom){
        double amount = this.hashMap.getOrDefault(toWhom,0.0);

        return amount;
    }
}
