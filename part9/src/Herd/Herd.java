package Herd;

import java.util.ArrayList;

public class Herd implements Movable{

    ArrayList<Organism> herd ;

    public Herd(){
        this. herd = new ArrayList<>();
    }

    public void addToHerd(Movable movable){
        Organism org = (Organism) movable;
        this.herd.add(org);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for(Organism e : this.herd){
            s.append(e.toString());
            s.append('\n');
        }
        return s.toString();
    }

    @Override
    public void move(int dx, int dy) {
        for(Organism e : this.herd){
            e.move(dx, dy);
        }

    }
}
