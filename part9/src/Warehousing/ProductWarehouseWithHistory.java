package Warehousing;

public class ProductWarehouseWithHistory extends ProductWarehouse{

    ChangeHistory history;

    public ProductWarehouseWithHistory(String productName, double capacity, double initialBalance) {
        super(productName, capacity);
        this.history = new ChangeHistory();
        this.setBalance(initialBalance);
        history.add(initialBalance);
    }

    public String history(){
        return this.history.toString();
    }

    @Override
    public void addToWarehouse(double amount) {
        super.addToWarehouse(amount);
        this.history.add(getBalance());
    }

    @Override
    public double takeFromWarehouse(double amount) {
        double temp = super.takeFromWarehouse(amount);
        this.history.add(getBalance());
        return temp;
    }

    public void printAnalysis(){
        System.out.println("Product Name: " + getName());
        System.out.println("History: " + this.history());
        System.out.println("Largest Amount of Product: " + this.history.maxValue());
        System.out.println("Smallest Amount of Product: " + this.history.minValue());
        System.out.println("Average: " + this.history.average());
    }
}
