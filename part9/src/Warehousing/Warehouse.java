package Warehousing;

public class Warehouse {
    private double capacity;
    private double balance;

    public Warehouse(double capacity) {
        if(capacity <= 0){
            this.capacity = 0;
        }
        else{
            this.capacity = capacity;
        }
    }

    public double getCapacity() {
        return capacity;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double howMuchSpaceLeft(){
        return capacity - balance;
    }

    public void addToWarehouse(double amount){
        if(amount > 0)
            this.balance += amount;

        if(this.balance > this.capacity)
            this.balance = this.capacity;
    }

    public double takeFromWarehouse(double amount){
        double given = 0;
        if(amount < 0)
            return 0;
        else{
            given = this.balance - amount;
            this.balance -= amount;
        }
        if(this.balance < 0){
            given = given + this.balance;
            this.balance = 0;
        }
        return given;
    }

    @Override
    public String toString(){
        return "Balance = " + this.balance + ", Space left " + this.howMuchSpaceLeft();
    }

}
