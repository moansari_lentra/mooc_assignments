package Person;

public class Student extends Person {
    private int credits;

    public Student(String name, String address) {
        super(name, address);
    }

    public void study(){
        this.credits++;
    }

    public String credits(){
        return Integer.toString(this.credits);
    }

    @Override
    public String toString(){
        return super.toString() + "\n" + "   Study Credits " + this.credits;
    }
}
