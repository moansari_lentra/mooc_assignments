package CargoHold;

import java.util.ArrayList;

public class Hold {
    int maxWeight;
    ArrayList<Suitcase> suitcases;

    public Hold(int maxWeight) {
        this.maxWeight = maxWeight;
        this.suitcases = new ArrayList<>();
    }

    public int sumOfWeight() {
        int sum = 0;
        for(Suitcase i : suitcases){
            sum += i.totalWeight();
        }
        return sum;
    }

    public void addSuitcase(Suitcase suitcase) {
        if(sumOfWeight() + suitcase.totalWeight() > maxWeight){
            System.out.println("Exceeding Limit!! Item not Added");
        }
        else
            this.suitcases.add(suitcase);
    }

    public void printItems() {
        for(Suitcase s : suitcases){
            s.printItems();
        }
    }

    public int totalWeight() {
        return sumOfWeight();
    }

//    public Item heaviestItem() {
//        if(this.items.isEmpty())
//            return null;
//
//        Item newItem = this.items.get(0);
//
//        for(Item i : items){
//            if(newItem.getWeight() < i.getWeight())
//                newItem = i;
//        }
//
//        return newItem;
//    }

    @Override
    public String toString() {
        if(suitcases.isEmpty())
            return "no suitcases (0 kg)";
        else if(suitcases.size() == 1)
            return "1 suitcase (" + sumOfWeight() + " kg)";
        else
            return this.suitcases.size() + " suitcases (" + sumOfWeight() + " kg)";
    }
}
