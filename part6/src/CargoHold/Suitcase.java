package CargoHold;

import java.util.ArrayList;

public class Suitcase {
    int maxWeight;
    ArrayList<Item> items;

    public Suitcase(int maxWeight) {
        this.maxWeight = maxWeight;
        this.items = new ArrayList<>();
    }

    public int sumOfWeight() {
        int sum = 0;
        for(Item i : items){
            sum += i.getWeight();
        }
        return sum;
    }

    public void addItem(Item item) {
        if(sumOfWeight() + item.getWeight() > maxWeight){
            System.out.println("Exceeding Limit!! Item not Added");
        }
        else
            this.items.add(item);
    }

    public void printItems() {
        for(Item i : items){
            System.out.println(i);
        }
    }

    public int totalWeight() {
        return sumOfWeight();
    }

    public Item heaviestItem() {
        if(this.items.isEmpty())
            return null;

        Item newItem = this.items.get(0);

        for(Item i : items){
            if(newItem.getWeight() < i.getWeight())
                newItem = i;
        }

        return newItem;
    }

    @Override
    public String toString() {
        if(items.isEmpty())
            return "no items (0 kg)";
        else if(items.size() == 1)
            return "1 item (" + sumOfWeight() + " kg)";
        else
            return this.items.size() + " items (" + sumOfWeight() + " kg)";
    }
}
