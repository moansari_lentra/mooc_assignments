import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest  {

    private Calculator calculator;

    @BeforeEach
    public void setUp(){
        calculator = new Calculator();
    }

    @Test
    public void calculatorInitialValueZero() {
        Assertions.assertEquals(0,calculator.getValue());
    }

    @Test
    public void valueWhenFiveAdded() {
        calculator.add(5);
        Assertions.assertEquals(5,calculator.getValue());
    }

    @Test
    public void valueWhenTwoSubtract() {
        calculator.subtract(2);
        Assertions.assertEquals(-2,calculator.getValue());
    }

}