package JokeManager;

import java.util.ArrayList;
import java.util.Random;

public class JokeManager {
    ArrayList<String> jokes;

    public JokeManager() {
        this.jokes = new ArrayList<>();
    }

    public void addJoke(String joke){
        this.jokes.add(joke);
    }

    public String drawJokes(){
        Random draw = new Random();
        int index = draw.nextInt(this.jokes.size());
        return this.jokes.get(index);
    }

    public void printJokes(){
        System.out.println("printing the jokes");
        for(String s : jokes){
            System.out.println(s);
        }
    }

}
