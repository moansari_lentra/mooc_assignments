package JokeManager;

import java.util.Scanner;

public class UserInterface {
    JokeManager manager;
    Scanner scanner;

    public UserInterface(JokeManager manager, Scanner scanner) {
        this.manager = manager;
        this.scanner = scanner;
    }

    public void start() {
        while(true){
            System.out.printf("Commands:\n1 - Add a Joke\n2 - Draw a Joke\n3 - List Jokes\nX - Stop\n");
            String choice = scanner.nextLine();

            if (choice.equals("X"))
                break;

            else if(choice.equals("1")){
                System.out.println("Write the joke to be added:");
                this.manager.addJoke(scanner.nextLine());
            }
            else if(choice.equals("2")){
                System.out.println("Drawing a Joke");
                if(this.manager.jokes.isEmpty())
                    System.out.println("Jokes are in Short supply");
                else {
                    System.out.println(this.manager.drawJokes());
                }
            }
            else if(choice.equals("3")){
                this.manager.printJokes();
            }
        }
    }
}
