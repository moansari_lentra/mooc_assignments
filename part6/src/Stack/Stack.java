package Stack;

import java.util.ArrayList;

public class Stack {
    ArrayList<String> stack;

    public Stack(){
        this.stack = new ArrayList<>();
    }

    public boolean isEmpty(){
        return this.stack.isEmpty();
    }

    public void add(String s){
        this.stack.add(s);
    }

    public String take(){
        String s = this.stack.get(this.stack.size() - 1);
        this.stack.remove(this.stack.size() - 1);
        return s;
    }
    public ArrayList<String> values(){
        return this.stack;
    }

}
