public class Main {
    public static void main(String[] args) {

        Person ada = new Person("Ada",3);
        Person antti = new Person("Antti",3);


        ada.printPerson();
        ada.growOlder();
        ada.printPerson();
        antti.printPerson();

    }
}
