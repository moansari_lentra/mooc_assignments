import java.util.ArrayList;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Person> persons = new ArrayList<>();
//        Person john = new Person("John");
//
//        persons.add(john);
//
//        persons.add(new Person("Matt"));
//        persons.add(new Person("Martin"));
//
//        for (Person i:
//             persons) {
//            System.out.println(i);
//        }

        while(true){
            System.out.println("Enter name");
            String name = scanner.nextLine();

            if(name.equals(""))
                break;

            System.out.println("Enter age:");
            int age = Integer.parseInt(scanner.nextLine());



            persons.add(new Person(name,age));
        }

        System.out.println("Total Persons : " + persons.size());
        System.out.println("Persons are:");

        for (Person i : persons){
            System.out.println(i);
            i.ageMoreThanTen();
        }

    }
}
