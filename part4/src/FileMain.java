import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class FileMain {
    public static void main(String[] args) {
        //read from user
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter name of file");
        String filename = scan.nextLine();
        filename = "/home/anas/Desktop/part4/src/" + filename;
        //System.out.println(filename);

        //
        ArrayList<String> lines = new ArrayList<>();

        try(Scanner scanner = new Scanner(Paths.get(filename))){
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                if(line.isEmpty()){
                    continue;
                }
                lines.add(line);
            }
        }
        catch (Exception e) {
            System.out.println("Error" + e.getMessage());;
        }
        System.out.println("Number of Lines :" + lines.size());
        //search by value
//        System.out.println("Search for:");
//        String search = scan.nextLine();
//
//        if(lines.contains(search)){
//            System.out.println("Found");
//        }
//        else
//            System.out.println("Not Found");
        //display
        for (String i : lines)
            System.out.println(i);
    }
}
