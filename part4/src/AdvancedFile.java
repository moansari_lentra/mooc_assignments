import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class AdvancedFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file name: ");
        String filename = scanner.nextLine();
        filename = "/home/anas/Desktop/part4/src/" + filename;

        ArrayList<Person> persons = new ArrayList<>();

        try (Scanner scanner2 = new Scanner(Paths.get(filename))){
            while(scanner2.hasNextLine()){
                String line = scanner2.nextLine();
                if(line.isEmpty()){
                    continue;
                }
                String[] lineArr = line.split(",");
                String name = lineArr[0];
                int age = Integer.parseInt(lineArr[1]);

                persons.add(new Person(name,age));
            }

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }

        for(Person i : persons){
            System.out.println(i);
        }
    }
}
