import java.util.Scanner;

public class Cubes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("Enter number(end to finish)");
            String s = scanner.nextLine();

            if(s.equals("end"))
                break;

            System.out.println(cube(Integer.valueOf(s)));
        }
    }

    public static int cube(int n){
        return n*n*n;
    }
}
