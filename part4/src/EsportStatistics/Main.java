package EsportStatistics;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        //System.out.println("Enter file name: ");
        //String filename = scanner.nextLine();
        //filename = "/home/anas/Desktop/part4/src/EsportStatistics/" + filename;

        ArrayList<String> file = readFile("/home/anas/Desktop/part4/src/EsportStatistics/MatchData.txt");
        //file data : home team,visiting team,home team points,visiting team points
        System.out.println("Enter team name: ");
        String team = scanner.nextLine();
        int count = 0;
        for(String i : file){
            if(i.contains(team)){
                count++;
            }
        }
        System.out.println("Games Played: " + count);
        int nWins = getWins(file,team);
        System.out.println("Wins: " + nWins);
        System.out.println("Losses: " + (count - nWins));

    }


    public static int getWins(ArrayList<String> s,String teamName){
        int numberOfWins = 0;
        for(String i : s){
            if(i.contains(teamName)){
                String[] str = i.split(",");
                if(str[0].equals(teamName)){
                    if((Integer.parseInt(str[2]) - Integer.parseInt(str[3])) > 0)
                        numberOfWins++;
                }
                else{
                    if((Integer.parseInt(str[3]) - Integer.parseInt(str[2])) > 0)
                        numberOfWins++;
                }
            }
        }

        return numberOfWins;
    }


    public static ArrayList<String> readFile(String filename){
        ArrayList<String> lines = new ArrayList<>();

        try (Scanner scanner = new Scanner(Paths.get(filename))){
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                if(line.isEmpty()){
                    continue;
                }
                lines.add(line);
            }

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return lines;
    }
}
