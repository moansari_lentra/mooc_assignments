package Books;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Book> books = new ArrayList<>();

        while(true){
            System.out.println("Enter name of the Book");
            String name = scanner.nextLine();

            if(name.equals("")){
                break;
            }

            System.out.println("Enter number of Pages");
            int n = Integer.parseInt(scanner.nextLine());

            System.out.println("Enter year of publication");
            int y = Integer.parseInt(scanner.nextLine());

            books.add(new Book(name,n,y));
        }

        System.out.printf("What should be printed:\n1.Everything\n2.Display by name\n3.Only Names\n4.Nothing\n");
        int choice = Integer.parseInt(scanner.nextLine());

        if(choice == 1){
            for(Book i : books){
                System.out.println(i);
            }
        }
        else if(choice == 2){
            System.out.println("Enter name of the book");
            String bookname = scanner.nextLine();

            for(Book i : books){
                if(bookname.equals(i.getTitle())){
                    System.out.println(i);
                }
            }
        }
        else if(choice == 3){
            for(Book i : books){
                System.out.println(i.getTitle());
            }
        }
        else{
            System.out.println("Nothing is here!");
        }
    }
}
