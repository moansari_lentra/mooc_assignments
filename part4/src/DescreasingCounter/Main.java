package DescreasingCounter;

public class Main {
    public static void main(String[] args) {
        DecreasingCounter decreasingCounter = new DecreasingCounter(100);

        decreasingCounter.printValue();

        decreasingCounter.decrement();
        decreasingCounter.decrement();

        decreasingCounter.printValue();

        decreasingCounter.reset();
        decreasingCounter.printValue();

        decreasingCounter.decrement();
        decreasingCounter.printValue();

        System.out.println(decreasingCounter);
    }
}
