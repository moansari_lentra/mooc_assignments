package DescreasingCounter;

public class DecreasingCounter {
    private int value;

    public DecreasingCounter(int initialValue){
        this.value = initialValue;
    }

    public void printValue(){
        System.out.println("Current value:" + this.value);
    }

    public void decrement(){
        if(this.value > 0) {
            this.value--;
            System.out.println("value decreased by one" + this.value);
        }
        //this.printValue();
    }

    public void reset(){
        this.value = 0;
        System.out.println("Value reset");
    }

    @Override
    public String toString() {
        return "DecreasingCounter{" +
                "value=" + value +
                '}';
    }
}
