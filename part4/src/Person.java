public class Person {
    private String name;
    private int age;
    private int weight;
    private int height;

    public Person(String name,int age){
        this.name = name;
        this.age = age;
        this.weight = 0;
        this.height = 0;
        System.out.println("--Object Created--");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void printPerson(){

        System.out.println(this.name + ", age " + this.age + " years");
    }

    public void growOlder(){
        this.age++;
        System.out.println("age increased");
    }

    public double bodyMassIndex() {
        double heightDivByHundred = this.height / 100.0;
        return this.weight / (heightDivByHundred * heightDivByHundred);
    }

    public void ageMoreThanTen(){
        if(this.age > 10)
            System.out.println(this.getName() + " has lived more than 10 years");
    }

    @Override
    public String toString() {
        return this.name + ", age " + this.age + " years";
    }
}
