package PaymentCard;

public class MultipleCards {
    public static void main(String[] args) {
        PaymentCard paul = new PaymentCard(20);
        PaymentCard matt = new PaymentCard(30);

        paul.eatHeartily();
        System.out.println("Paul : " + paul);

        matt.eatAffordably();
        System.out.println("matt : " + matt);

        paul.addMoney(20);
        matt.eatHeartily();
        //
        System.out.println("Paul : " + paul);
        System.out.println("matt : " + matt);

        paul.eatAffordably();
        paul.eatAffordably();

        matt.addMoney(50);

        System.out.println("Paul : " + paul);
        System.out.println("matt : " + matt);

    }
}
