import java.util.Scanner;

public class PrintingStars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //printStars
        System.out.println("Enter no of Stars : ");
        int n = Integer.parseInt(scanner.nextLine());
        printStars(n);
        //System.out.println("");

        //Leaning Traingle
        System.out.println("Enter size:");
        n = Integer.parseInt(scanner.nextLine());
        printLeaningTriangle(n);

        //Christmas Tree
        System.out.println("Enter Height");
        n = Integer.parseInt(scanner.nextLine());
        printChristmasTree(n);

    }
    public static void printStringNTimes(int num,String A){
        for(int i = 0; i < num; i++)
            System.out.print(A);
    }
    public static void printStars(int num){
        printStringNTimes(num,"*");
        System.out.println("");
    }
    public static void printLeaningTriangle(int size){
        for(int i = 0;i < size; ++i){
            printStringNTimes(size - i - 1, " ");
            printStars(i+1);
        }
    }
    public static void printChristmasTree(int height){
        for(int i = 0;i < height; ++i){
            printStringNTimes(height - i - 1, " ");
            printStars(((i * 2) + 1));
        }
        for(int j = 0;j < 3; j++){
            printStringNTimes(height - 2," ");
            printStars(3);
        }
    }
}
