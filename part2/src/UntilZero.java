import java.util.Scanner;

public class UntilZero {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = 0;
        int sum = 0;
        int number;

        while(true){
            System.out.println("Give a number");
            number = Integer.parseInt(scanner.nextLine());
            if(number == 0)
                break;
            else {
                count++;
                sum+=number;
                System.out.println(number);
            }
        }
        System.out.println("Count: " + count);
        System.out.println("Sum: " + sum);
    }
}
