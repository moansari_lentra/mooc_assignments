import java.util.Scanner;

public class OnlyPositives {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number;

        while(true){
            System.out.println("Enter Number");
            number = Integer.parseInt(scanner.nextLine());

            if(number == 0)
                break;
            else if(number < 0 ) {
                System.out.println("Unsuitable");
                continue;
            }
            else
                System.out.println("VAL:" + (number * number));
        }
    }
}
