import java.util.Scanner;

public class intro {
    public static void main(String[] args) {

        //Hello world!
        System.out.println("Hello world!!");

        //Reading and Printing
        Scanner scan = new Scanner(System.in);
        {
            System.out.println("Write a Message");
            String msg = scan.nextLine();
            System.out.println("Message is :" + msg);
        }

        //concatenate
        String temp = "Two";
        System.out.println("Add" + temp + "Strings");

        //variables:naming variable
        {
            float radius = 2.0f;
            float pi = 3.141f;
            double area = pi * radius * radius;
            System.out.println("Area : " + area);
        }

        //reading other data types
        {
            System.out.println("Enter Integer:");
            int someInteger = Integer.parseInt(scan.nextLine());
            System.out.println("you entered: " + someInteger);

            System.out.println("Enter Double");
            double someDouble = Double.parseDouble(scan.nextLine());
            System.out.println("you entered: " + someDouble);

            System.out.println("Enter Boolean");
            boolean someBoolean = Boolean.parseBoolean(scan.nextLine());
            System.out.println("you entered: " + someBoolean);

        }

    }
}
