import java.util.Scanner;

public class GiftTax {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Value:");
        int valueOfGift = Integer.parseInt(scanner.nextLine());

        if(valueOfGift < 5000)
            System.out.println("No Gift Tax");
        else if(valueOfGift < 25000)
            System.out.println("Tax :" + tax(valueOfGift,5000,100,8));
        else if(valueOfGift < 55000)
            System.out.println("Tax :" + tax(valueOfGift,25000,1700,10));
        else if(valueOfGift < 200000)
            System.out.println("Tax :" + tax(valueOfGift,55000,4700,12));
        else if(valueOfGift < 1000000)
            System.out.println("Tax :" + tax(valueOfGift,200000,22100,15));
        else
            System.out.println("Tax :" + tax(valueOfGift,1000000,147100,17));

    }
    public static double tax(int value,int lowerLimit,int initialTax,double rate){
        return initialTax + ((value - lowerLimit) * rate * 0.01);
    }
}
