import java.util.Scanner;

public class UsingWhileLoop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int i = -2;
        while(i < 0){
            System.out.println("This is while loop");
            i++;
        }
    }
}
