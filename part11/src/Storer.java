import java.io.FileWriter;
import java.io.PrintWriter;

public class Storer {
    public void writeToFile(String filename, String s ) throws Exception{
        PrintWriter writer = new PrintWriter(filename);
        writer.append(s);
        writer.flush();
    }

    public void addToFile(String filename, String s ) throws Exception{
        FileWriter writer = new FileWriter(filename,true);
        writer.append(s);
        writer.close();
    }

}
