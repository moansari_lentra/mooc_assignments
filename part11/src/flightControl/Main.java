package flightControl;

import flightControl.logic.FlightControl;
import flightControl.ui.TextUI;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        FlightControl flightControl = new FlightControl();

        TextUI t = new  TextUI(flightControl,scanner);

        t.start();
    }
}
