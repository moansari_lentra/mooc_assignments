import java.util.Scanner;

public class LiquidContainer {
    public static void main(String[] args) {
        int firstValue = 0;
        int secondValue = 0;

        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("First: " + firstValue + "/100");
            System.out.println("Second: " + secondValue + "/100");
            String line = scanner.nextLine();

            if(line.equals("q"))
                break;

            String[] inputs = line.split(" ");
            String command = inputs[0];
            int amount = Integer.parseInt(inputs[1]);

            if(amount < 0)
                continue;

            if(command.equals("add")){
                firstValue += amount;
                if(firstValue > 100)
                    firstValue = 100;
            }
            else if(command.equals("move")){
                secondValue += amount;
                firstValue -= amount;
                if(firstValue < 0 && secondValue > 100){
                    secondValue = 100;
                    firstValue = 0;
                }
                else if(firstValue < 0){
                    secondValue += firstValue;
                    firstValue = 0;
                }
                else if(secondValue > 100){
                    secondValue = 100;
                }

            }
            else if(command.equals("remove")){
                secondValue -= amount;
                if(secondValue < 0)
                    secondValue = 0;
            }

        }
    }
}
