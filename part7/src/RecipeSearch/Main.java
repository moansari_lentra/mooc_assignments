package RecipeSearch;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        String filename = "/home/anas/Desktop/part7/src/RecipeSearch/recipes.txt";
        Scanner scan = new Scanner(System.in);

        ArrayList<Recipe> recipes = new ArrayList<>();

        Recipe recipe = new Recipe();
        ArrayList<String> s = new ArrayList<>();
        String line;

        try ( Scanner scanner = new Scanner(Paths.get(filename)) ) {
            while(scanner.hasNextLine()){
                line = scanner.nextLine();
                if(line.isEmpty()){
                    //System.out.println("----Empty----");
                    //System.out.println(s.get(0) + " " + s.get(1));
                    recipe.setRecipeName(s.get(0));
                    //System.out.println("recipe added");
                    recipe.setCookingTime(s.get(1));
                    //System.out.println("cooking time added");
                    s.remove(0);
                    s.remove(0);
                    //System.out.println(s);
                    recipe.setIngredients(s);
//                    s = new ArrayList<>();
                    recipes.add(recipe);
                    recipe = new Recipe();
                    s.clear();
                    continue;
                }
                s.add(line);

            }
        } catch ( IOException e ) {
            System.out.println("Error: " + e.getMessage());}

//        for(Recipe i : recipes){
//            i.printRecipe();
//        }
//        System.out.println(s);

        while(true){
            System.out.printf("----Search: ----\n1.By Name\n2.By Cooking Time\n3.By Ingredients\n4.Exit\n");
            String c =scan.nextLine();

            if(c.equals("4"))
                break;
            else if(c.equals("1")){
                System.out.println("Enter name :");
                String name = scan.nextLine();
                System.out.println("Recipe name");
                for(Recipe r : recipes){
                    if(r.recipeName.contains(name))
                        System.out.println(r.recipeName);
                }
            }
            else if(c.equals("2")){
                System.out.println("Enter time :");
                String name = scan.nextLine();
                System.out.println("Recipe name");
                for(Recipe r : recipes){
                    if(r.cookingTime.equals(name))
                        System.out.println(r.recipeName);
                }
            }
            else if(c.equals("3")){
                System.out.println("Enter ingredient name :");
                String name = scan.nextLine();
                System.out.println("Recipe name");
                for(Recipe r : recipes){
                    if(r.ingredients.contains(name))
                        System.out.println(r.recipeName);
                }
            }
        }
    }
}
