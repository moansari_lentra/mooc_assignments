package RecipeSearch;

import java.io.IOException;
import java.util.ArrayList;

public class Recipe {
    String recipeName;
    String cookingTime;
    ArrayList<String> ingredients;

    public Recipe(){
        //System.out.println("---Recipe Object Created---");
        this.ingredients = new ArrayList<>();
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public void setCookingTime(String cookingTime) {
        this.cookingTime = cookingTime;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients.addAll(ingredients);
    }

    //    public void readRecipe(){
//        System.out.println("Reading Recipe....");
//        while(this.fileReader.scanner.hasNextLine()){
//            this.recipeName = this.fileReader.scanner.nextLine();
//            this.cookingTime = Integer.parseInt(this.fileReader.scanner.nextLine());
//            String line = this.fileReader.scanner.nextLine();
//            while(!(line.isEmpty())){
//                ingredients.add(line);
//            }
//        }
//    }
//
    public void printRecipe(){
        System.out.println(this.recipeName + ",Cooking Time: " + this.cookingTime);
        System.out.println("Ingredients: " + ingredients);
    }
}
