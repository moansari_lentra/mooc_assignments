package RecipeSearch;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileReader {
    Scanner scanner;
    String filename;

    public FileReader(String filename) throws IOException {
        System.out.println("---FileReader Object Created---");
        this.filename = filename;
        this.scanner = new Scanner(Paths.get(filename));
    }

    public Scanner getScanner() {
        return scanner;
    }
}
