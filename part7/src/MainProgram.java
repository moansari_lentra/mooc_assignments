import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainProgram {
    public static void main(String[] args) {

        int[] num = {6, 5, 8, 7, 11};
        System.out.println("Smallest: " + MainProgram.smallest(num));
        System.out.println("Index of the smallest number: " + MainProgram.indexOfSmallest(num));

        int[] numbers = {-1, 6, 9, 8, 12};
        //System.out.println(MainProgram.indexOfSmallestFrom(numbers, 0));
        //System.out.println(MainProgram.indexOfSmallestFrom(numbers, 1));
        //System.out.println(MainProgram.indexOfSmallestFrom(numbers, 2));

        //System.out.println(Arrays.toString(numbers));

        MainProgram.swap(numbers, 1, 0);
        //System.out.println(Arrays.toString(numbers));

        MainProgram.swap(numbers, 0, 3);
        //System.out.println(Arrays.toString(numbers));

        int[] number = {8, 3, 7, 9, 1, 2, 4};
        //MainProgram.sort(number);

        int[] numbe = {8, 3, 7, 9, 1, 2, 4};
//        System.out.println(Arrays.toString(numbe));
//        Arrays.sort(numbe);
//        System.out.println(Arrays.toString(numbe));

//        ArrayList<Integer> n = new ArrayList<>();
//        n.add(8);
//        n.add(3);
//        n.add(7);
//        System.out.println(n);
//        Collections.sort(n);
//        System.out.println(n);

//        String[] s = {"anas","absd","jkls"};
//        System.out.println(Arrays.toString(s));
//        Arrays.sort(s);
//        System.out.println(Arrays.toString(s));
//
        ArrayList<String> s = new ArrayList<>();
        s.add("anas");
        s.add("afsd");
        s.add("ert");
        s.add("aaa");
        System.out.println(s);
        Collections.sort(s);
        System.out.println(s);
    }
    public static int smallest(int[] array){
        int smallest = array[0];
        for(int i : array){
            if(smallest > i)
                smallest = i;
        }
        return smallest;
    }

    public static int indexOfSmallest(int[] array){
        int smallest = 0;
        for(int i = 1; i < array.length; i++){
            if(array[smallest] > array[i])
                smallest = i;
        }
        return smallest;
    }

    public static int indexOfSmallestFrom(int[] array,int startIndex){
        int smallest = startIndex;
        for(int i = startIndex + 1; i < array.length; i++){
            if(array[smallest] > array[i])
                smallest = i;
        }
        return smallest;
    }
    public static void swap(int[] array,int index1,int index2){
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    public static void sort(int[] array) {
        for(int i = 0; i < array.length; i++){
            System.out.println(Arrays.toString(array));
            swap(array,i,indexOfSmallestFrom(array,i));
        }
    }
}
